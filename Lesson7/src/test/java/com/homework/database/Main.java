package com.homework.database;

import com.mysql.fabric.jdbc.FabricMySQLDriver;
import java.sql.*;

public class Main {

    private static final String url = "jdbc:mysql://localhost:3306/homework?useSSL=false";
    private static final String username = "root";
    private static final String password = "root";

    public static void main(String[] args) {
        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(url, username, password);
             Statement statement = connection.createStatement()) {

            //Создание и редактирование
    /*statement.execute("INSERT INTO homework.user(user_name, group_number) VALUES ('Lucky Strike', 48)");
    statement.executeUpdate("update homework.user set group_number=22, user_name='Dreamy' where user_id=27");*/

            //Пакетное создание
    /*statement.addBatch("INSERT INTO homework.user(user_name, group_number) VALUES ('Person to delete', 234)");
    statement.addBatch("INSERT INTO homework.user(user_name, group_number) VALUES ('Person to delete', 345)");
    statement.addBatch("INSERT INTO homework.user(user_name, group_number) VALUES ('Person to delete', 456)");
    statement.addBatch("INSERT INTO homework.user(user_name, group_number) VALUES ('Person to delete', 567)");
    statement.executeBatch();
    statement.clearBatch();*/

            //Удаление
    /*statement.executeUpdate("delete from homework.user where user_id>14");
        */

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}